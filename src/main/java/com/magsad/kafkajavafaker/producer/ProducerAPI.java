package com.magsad.kafkajavafaker.producer;

import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
@RequiredArgsConstructor
public class ProducerAPI {

        Faker faker = Faker.instance();;

        @EventListener(ApplicationStartedEvent.class)
        public void generate(){
            Properties properties = new Properties();
            properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
            properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
            properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            KafkaProducer<Integer,String> producer = new KafkaProducer<Integer, String>(properties);

            for (int i=1;i<100;i++){
                ProducerRecord<Integer, String> record = new ProducerRecord<>("hobbit-text-topic",i, faker.hobbit().quote() + " - " + i);
                producer.send(record);
            }
            producer.close();
        }
}
