package com.magsad.kafkajavafaker;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.config.TopicBuilder;

@SpringBootApplication
public class KafkaJavafakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaJavafakerApplication.class, args);
    }

//    @Bean
//    NewTopic hobbit(){
//        return TopicBuilder.name("hobbit-text-topic").replicas(3).partitions(15).build();
//    }
}
